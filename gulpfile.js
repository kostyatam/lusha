var gulp = require('gulp');
var styl = require('gulp-stylus');
var config = require('./config');
var open = require('gulp-open');
var webserver = require('gulp-webserver')

gulp.task('build:styles', function () {
	return gulp.src('src/styles/styles.styl')
			.pipe(styl())
			.pipe(gulp.dest('./public/css'))
})
gulp.task('watch:styles', function () {
	gulp.watch('src/styles/*.styl', ['build:styles']);
})

gulp.task('build:html', function () {
	return gulp.src('src/*.html')
			.pipe(gulp.dest('./public'))
})

gulp.task('watch:html', function () {
	gulp.watch('src/*.html', ['build:html']);
})

gulp.task('start:server', ['build:all'], function () {
	return gulp.src('public')
		.pipe(webserver({
			host: config.get('host'),
			port: config.get('port')
		}))
})

gulp.task('start:browser', ['start:server'], function () {
	return gulp.src(__filename)
		.pipe(open({
			uri: config.get('uri')
		}))
})

gulp.task('build:all', ['build:styles', 'build:html']);

gulp.task('watch:all', ['watch:styles', 'watch:html']);

gulp.task('run:dev', ['watch:all', 'start:browser']);

gulp.task('default', ['start:browser']);