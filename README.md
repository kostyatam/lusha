#### 1. Install gulp globally:

```sh
$ npm install --global gulp
```

#### 2. Install node modules:

```sh
$ npm install
```

#### 3. Start project:

```sh
$ gulp
```

#### 4. For development use another gulp task:

```sh
$ gulp run:dev
```