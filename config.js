module.exports = {
	get: function (prop) {
		switch (prop) {
			case 'uri':
				return ['http://', this.host, ':', this.port].join('');
			case 'public':
				return 'public'
			default:
				return this[prop]
		}
	},
	port: 8189,
	host: 'localhost'
}